package hash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"hash"
	"sync"
)

// NewHMAC creates and return a new hmac obj
func NewHMAC(key string) HMAC {
	h := hmac.New(sha256.New, []byte(key))
	var mut sync.Mutex

	return HMAC{
		hmac: h,
		mu:   &mut,
	}

}

// HMAC is a wrapper around the crypto/hmac
type HMAC struct {
	hmac hash.Hash
	mu   *sync.Mutex
}

func (h HMAC) Hash(input string) string {
	h.mu.Lock()
	h.hmac.Reset()
	h.hmac.Write([]byte(input))
	b := h.hmac.Sum(nil)
	str := base64.URLEncoding.EncodeToString(b)
	h.mu.Unlock()
	return str
}
