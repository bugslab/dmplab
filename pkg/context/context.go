package context

import (
	"context"
	"dmplab/internal/models"
)

type privateKey string

const (
	userKey  privateKey = "user"
	videoKey privateKey = "video"
)

func WithUser(ctx context.Context, user *models.User) context.Context {
	return context.WithValue(ctx, userKey, user)
}

func User(ctx context.Context) *models.User {
	if temp := ctx.Value(userKey); temp != nil {
		if user, ok := temp.(*models.User); ok {
			return user
		}
	}
	return nil
}

func WithVideo(ctx context.Context, video *models.Video) context.Context {
	return context.WithValue(ctx, videoKey, video)
}

func Video(ctx context.Context) *models.Video {
	if temp := ctx.Value(videoKey); temp != nil {
		if video, ok := temp.(*models.Video); ok {
			return video
		}
	}
	return nil
}
