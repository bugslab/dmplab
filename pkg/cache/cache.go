package cache

import (
	"github.com/patrickmn/go-cache"
	"time"
)

type CacheService interface {
	Add(string, interface{}, time.Duration) error
	Get(string) (interface{}, bool)
}

var _ CacheService = &cacheService{}

type cacheService struct {
	cache.Cache
}

func NewCacheService() CacheService {
	ch := cache.New(15*time.Minute, 20*time.Minute)
	return ch
}
