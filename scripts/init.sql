CREATE DATABASE dmplab_dev;
CREATE DATABASE dmplab_log;

\c dmplab_log;

CREATE TABLE log (
    id BIGSERIAL PRIMARY KEY,
    data jsonb
);

\c dmplab_dev;

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    deleted_at TIMESTAMP WITH TIME ZONE,
    name VARCHAR(32) NOT NULL,
    email VARCHAR(64) NOT NULL,
    role INTEGER NOT NULL,
    down_remaining INTEGER NOT NULL,
    password_hash TEXT NOT NULL,
    remember_hash TEXT NOT NULL
);

CREATE UNIQUE INDEX uix_users_email ON "users" ("email");
CREATE UNIQUE INDEX uix_users_remember_hash ON "users" ("remember_hash");
CREATE INDEX idx_users_deleted_at  ON "users" ("deleted_at");

CREATE TABLE videos (
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    deleted_at TIMESTAMP WITH TIME ZONE,
    filename VARCHAR(128) NOT NULL,
    title VARCHAR(128) NOT NULL,
    description VARCHAR(10240),
    tags VARCHAR(16)[] NOT NULL CHECK (array_ndims(tags) = 1 AND array_length(tags, 1) <= 32),
    md5 BYTEA NOT NULL,
    size INTEGER NOT NULL,
    status INTEGER NOT NULL,
    tusd_id TEXT,
    video_pub_link TEXT,
    screen_pub_link TEXT,
    thumb_pub_link TEXT,
    user_id BIGINT NOT NULL REFERENCES users (id)
 );

CREATE UNIQUE INDEX uix_videos_md5 ON "videos" ("md5");
CREATE INDEX idx_videos_tags ON "videos" ("tags");
CREATE INDEX idx_videos_deleted_at ON "videos" ("deleted_at");
CREATE INDEX idx_videos_filename ON "videos" ("filename");
CREATE INDEX idx_videos_user_id ON "videos" ("user_id");

CREATE TABLE about (
    id BIGSERIAL PRIMARY KEY,
    created_at TIMESTAMP WITH TIME ZONE,
    updated_at TIMESTAMP WITH TIME ZONE,
    deleted_at TIMESTAMP WITH TIME ZONE,
    title VARCHAR(256) NOT NULL,
    description VARCHAR(10240) NOT NULL,
    thumbnail TEXT DEFAULT '',
    category VARCHAR(64) NOT NULL
);
