GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=dmplab
ROOT:=$(shell pwd)


dev: build
	./scripts/makeconfig.sh ./config/rclone.conf ./config/config.json
	docker-compose up --build

npm:
	npm run build --prefix ui/api/
	rice embed-go -v -i ./internal/controllers

build: protocfg
	$(GOCMD) mod tidy
	$(GOBUILD) -o ./deploy/api ./cmd/api/api.go
	$(GOBUILD) -o ./deploy/ui ./cmd/ui/ui.go
	$(GOBUILD) -o ./deploy/auth ./internal/proto/servers/auth/main.go
	$(GOBUILD) -o ./deploy/config ./internal/proto/servers/config/main.go
	$(GOBUILD) -o ./deploy/log ./internal/proto/servers/log/main.go
	$(GOBUILD) -o ./deploy/tusd ./internal/proto/clients/hook/main.go
	$(GOBUILD) -o ./deploy/hook ./internal/proto/servers/hook/main.go
	cd $(ROOT)/third_party/caddy/caddy/ && $(GOBUILD) -o $(ROOT)/deploy/caddy .

protocfg:
	mkdir -p $(ROOT)/internal/proto/{contracts,servers,clients}/{config,logger,hook,auth}
	mkdir -p $(ROOT)/ui/api/src/proto/logger
	./scripts/makeconfig.sh ./config/rclone.conf ./config/config.json
	cd $(ROOT)/proto && protoc --go_out=plugins=grpc:../internal/proto/contracts/config/ --go_opt=paths=source_relative config.proto
	cd $(ROOT)/proto && protoc --go_out=plugins=grpc:../internal/proto/contracts/logger/ --go_opt=paths=source_relative logger.proto
	cd $(ROOT)/proto && protoc --js_out=import_style=commonjs:../ui/api/src/proto/logger  --grpc-web_out=import_style=typescript,mode=grpcwebtext:../ui/api/src/proto/logger logger.proto
	cd $(ROOT)/proto && protoc --go_out=plugins=grpc:../internal/proto/contracts/hook/ --go_opt=paths=source_relative hook.proto
	cd $(ROOT)/proto && protoc --go_out=plugins=grpc:../internal/proto/contracts/auth/ --go_opt=paths=source_relative auth.proto


api:
	$(GOBUILD) -o ./deploy/api ./cmd/api/api.go
	docker-compose up -d --no-deps --build dmp-api

db:
	docker-compose up -d --no-deps --build dmp-db

log:
	$(GOBUILD) -o ./deploy/log ./internal/proto/servers/log/main.go
	docker-compose up -d --no-deps --build dmp-log

uinterface:
	$(GOBUILD) -o ./deploy/ui ./cmd/ui/ui.go
	docker-compose up -d --no-deps --build dmp-ui

hook:
	$(GOBUILD) -o ./deploy/hook ./internal/proto/servers/hook/main.go
	docker-compose up -d --no-deps --build dmp-hook

auth:
	$(GOBUILD) -o ./deploy/auth ./internal/proto/servers/auth/main.go
	docker-compose up -d --no-deps --build dmp-auth

tusd:
	$(GOBUILD) -o ./deploy/tusd ./internal/proto/clients/hook/main.go
	docker-compose up -d --no-deps --build dmp-tusd

caddy:
	cd $(ROOT)/third_party/caddy/caddy/ && $(GOBUILD) -o $(ROOT)/deploy/caddy .
	docker-compose up -d --no-deps --build caddy
