package controllers

import (
	"dmplab/internal/models"
	"dmplab/internal/views"
	"encoding/json"
	"errors"
	"image"
	"image/jpeg"
	"io"
	"net/http"
	"os"
	"path"
	"strconv"
	"time"

	// registering codec

	_ "image/gif"
	_ "image/png"
)

type AboutAPI struct {
	AboutViewAPI *views.API
	as           models.AboutService
}

func NewAboutAPI(as models.AboutService) *AboutAPI {
	return &AboutAPI{
		AboutViewAPI: &views.API{},
		as:           as,
	}
}

func (aa *AboutAPI) GetAnnouncements(w http.ResponseWriter, r *http.Request) {
	var vd views.Data
	announcements, err := aa.as.ByMostRecentYield()
	if err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	vd.Yield = announcements
	aa.AboutViewAPI.Render(w, r, vd)
}

func (aa *AboutAPI) Create(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	about := new(models.About)
	if err := json.NewDecoder(r.Body).Decode(&about); err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	if err := aa.as.Create(about); err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}
	vd.Yield = about.ID

	vd.AlertSuccess("Announcement created successfully!")

	aa.AboutViewAPI.Render(w, r, vd)
}

func (aa *AboutAPI) CreateImage(w http.ResponseWriter, r *http.Request) {

	var vd views.Data

	if err := r.ParseMultipartForm(10 << 20); err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	strID := r.FormValue("id")
	if strID == "" {
		vd.SetAlert(errors.New("invalid id"))
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	id, err := toUint(strID)
	if err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	file, _, err := r.FormFile("file")
	if err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}
	defer file.Close()

	image, ext, err := image.Decode(file)
	if err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	extension := "jpeg"
	if ext == "gif" {
		extension = ext
	}
	filename := strconv.FormatInt(time.Now().UnixNano(), 10) + "." + extension
	path := path.Join("public", filename)
	wfile, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}
	defer wfile.Close()

	if ext == "gif" {
		_, err := file.Seek(0, io.SeekStart)
		if err != nil {
			vd.SetAlert(err)
			aa.AboutViewAPI.Render(w, r, vd)
			return
		}
		_, err = io.Copy(wfile, file)
		if err != nil {
			vd.SetAlert(err)
			aa.AboutViewAPI.Render(w, r, vd)
			return
		}
	} else {
		if err := jpeg.Encode(wfile, image, &jpeg.Options{Quality: jpeg.DefaultQuality}); err != nil {
			vd.SetAlert(err)
			aa.AboutViewAPI.Render(w, r, vd)
			return
		}
	}
	about := &models.About{
		Model: models.Model{
			ID: id,
		},
		Thumbnail: filename,
	}

	if err := aa.as.UpdateImage(about); err != nil {
		vd.SetAlert(err)
		aa.AboutViewAPI.Render(w, r, vd)
		return
	}

	vd.AlertSuccess("image uploaded successfully!")
	aa.AboutViewAPI.Render(w, r, vd)
}
