package controllers

import (
	"net/http"
	"strings"

	rice "github.com/GeertJohan/go.rice"
	"github.com/go-chi/chi"
)

// FileServerSPA will help to serve single page application (vuejs).
// The `public` parameter is the url path that the index will be servede
func FileServerSPA(router chi.Router, public string) {
	if strings.ContainsAny(public, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	box, err := rice.FindBox("../../ui/dist/api/")
	if err != nil {
		panic(err)
	}

	boxf := http.FileServer(box.HTTPBox())
	boxf = http.StripPrefix(public, boxf)
	fs := http.FileServer(FileSystem{http.Dir("./public")})
	fs = http.StripPrefix("/assets/thumbnail/", fs)

	if public != "/" && public[len(public)-1] != '/' {
		router.Get(public, http.RedirectHandler(public+"/", http.StatusMovedPermanently).ServeHTTP)
		public += "/"
	}

	router.Get(public+"*", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		file := strings.Replace(r.RequestURI, public, "/", 1)
		if strings.HasPrefix(file, "/assets/thumbnail/") {
			fs.ServeHTTP(w, r)
			return
		}
		f, err := box.Open(file)
		if err != nil {

			if strings.HasPrefix(r.RequestURI, "/assets") {
				w.Write([]byte("Nothing to see here!"))
				return

			}

			bs, _ := box.Bytes("index.html")
			w.Header().Set("Content-Type", "text/html")
			w.Write(bs)
			return
		}

		if stat, _ := f.Stat(); stat.IsDir() && strings.HasPrefix(r.RequestURI, "/assets") {
			w.Write([]byte("Nothing to see here!"))
			return

		}
		boxf.ServeHTTP(w, r)
	}))
}

type FileSystem struct {
	fs http.FileSystem
}

func (fs FileSystem) Open(path string) (http.File, error) {
	f, err := fs.fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, err := f.Stat()
	if err != nil {
		return nil, err
	}
	if s.IsDir() {
		index := strings.TrimSuffix(path, "/") + "/index.html"
		if _, err := fs.fs.Open(index); err != nil {
			return nil, err
		}
	}

	return f, nil
}
