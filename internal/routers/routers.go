package routers

import (
	mw  "dmplab/pkg/middleware"
	"net/http"

	"dmplab/internal/controllers"

	"github.com/go-chi/chi"
)

func WebAPIRoutes(C *controllers.ControllerServices) chi.Router {

	router := chi.NewRouter()

	router.Post("/login", C.UsersAPI.Login)
	router.Post("/register", C.UsersAPI.Register)

	router.Route("/", func(r chi.Router) {

		r.Use(mw.RequiredUser)
		r.Head("/logout", C.UsersAPI.Logout)
		r.Get("/credentials", C.UsersAPI.GetDecryptKeys)
		r.Get("/about", C.AboutAPI.GetAnnouncements)
		r.Get("/uservideos", C.VideoAPI.GetUserVideos)
		r.Route("/admin", func(r chi.Router) {
			r.Use(mw.RequireAdmin)
			r.Get("/dashboard/user", C.UsersAPI.UserDashboard)
			r.Put("/dashboard/user", C.UsersAPI.UpdateUser)
			r.Get("/dashboard/video", C.VideoAPI.VideoDashboard)
			r.Put("/dashboard/video", C.VideoAPI.AdminUpdateVideo)
			// r.Put("/dashboard/video", C.UsersAPI.UpdateUser)
			r.Post("/about", C.AboutAPI.Create)
			r.Put("/about", C.AboutAPI.CreateImage)
		})
		r.Route("/videos", func(r chi.Router) {
			r.Get("/", C.VideoAPI.GetVideos)
			r.Get("/{videoID}", C.VideoAPI.GetVideo)
		})
		r.Route("/tags", func(r chi.Router) {
			r.Get("/", C.VideoAPI.GetAllTags)
			r.Get("/{tag}", C.VideoAPI.GetVideosByTag)
		})
		r.Route("/down", func(r chi.Router) {
			r.Get("/{videoID}", C.VideoAPI.GetVideoDownloadURL)
		})
	})

	return router
}

func favicon(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/x-icon")
	w.Header().Set("Cache-Control", "public, max-age=7776000")

	http.ServeFile(w, r, "/favicon.ico")
}

func serviceWork(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/javascript")
	http.ServeFile(w, r, "/sw.js")
}
