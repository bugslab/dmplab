package views

import (
	"dmplab/internal/errors"
	"dmplab/internal/models"
	"dmplab/internal/proto/clients/log"
	"net/http"
)

const (
	AlertLvlError   = "error"
	AlerLvlWarning  = "warning"
	AlertLvlInfo    = "primary"
	AlertLvlSuccess = "success"
)

type Alert struct {
	Level   string `json:"level,omitempty"`
	Message string `json:"message,omitempty"`
}

type Data struct {
	Alert  *Alert       `json:"alert,omitempty"`
	User   *models.User `json:"user,omitempty"`
	Yield  interface{}  `json:"yield,omitempty"`
	Status int          `json:"-"`
}

func (d *Data) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (d *Data) SetStatus(status int) {
	d.Status = status
}

func (d *Data) SetAlert(err error) {

	var id int
	if d.User != nil {
		id = int(d.User.ID)
	}

	var pub *errors.ModelError
	if errors.As(err, &pub) {

		log.Warn(id, pub.Error())

		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: pub.Public(),
		}
		d.Status = pub.Status.ToHTTP()

	} else {
		log.Error(id, err.Error())
		d.Alert = &Alert{
			Level:   AlertLvlError,
			Message: errors.GenericError.Error(),
		}
		if d.Status < 400 {
			d.SetStatus(http.StatusInternalServerError)
		}
	}
}

func (d *Data) AlertSuccess(msg string) {
	d.Alert = &Alert{
		Level:   AlertLvlSuccess,
		Message: msg,
	}
}

func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLvlError,
		Message: msg,
	}
}
