package views

import (
	rice "github.com/GeertJohan/go.rice"
)

type ViewServicesConf func(*ViewServices) error

// WithFolderPath will embed all files in the dist/ folder into the final binary file.
func WithFolderPath() ViewServicesConf {
	return func(v *ViewServices) error {

		// Never use variable here, only string literal
		bx, err := rice.FindBox("../../ui/dist/")
		if err != nil {
			return err
		}
		v.Box = bx
		return nil
	}
}

type ViewServices struct {
	Box *rice.Box
}
