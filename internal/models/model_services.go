package models

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type ModelServicesConf func(*ModelServices) error

func WithDatabase(dialect, conInfo string) ModelServicesConf {
	return func(s *ModelServices) error {
		db, err := sqlx.Connect(dialect, conInfo)
		if err != nil {
			return err
		}
		s.db = db
		return nil
	}
}

func WithUser(hmac, pepper string) ModelServicesConf {
	return func(s *ModelServices) error {
		s.User = newUserService(s.db, hmac, pepper)
		return nil
	}
}

func WithVideo() ModelServicesConf {
	return func(s *ModelServices) error {
		s.Video = newVideoService(s.db)
		return nil
	}
}

func WithRRC() ModelServicesConf {
	return func(s *ModelServices) error {
		s.RRC = newRRCService()
		return nil
	}
}

func WithAbout() ModelServicesConf {
	return func(s *ModelServices) error {
		s.About = newAboutService(s.db)
		return nil
	}
}

func NewModelServices(cfgs ...ModelServicesConf) (*ModelServices, error) {
	var s ModelServices
	for _, cfg := range cfgs {
		if err := cfg(&s); err != nil {
			return nil, err
		}
	}

	return &s, nil
}

type ModelServices struct {
	User  UserService
	Video VideoService
	RRC   RRCService
	About AboutService
	db    *sqlx.DB
}

func (s *ModelServices) Close() error {
	return s.db.Close()
}
