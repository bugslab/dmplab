package models

import (
	"database/sql"
	"dmplab/internal/errors"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
)

type About struct {
	Model
	Title       string `db:"title" json:"title"`
	Description string `db:"description" json:"description"`
	Thumbnail   string `db:"thumbnail" json:"thumbnail"`
	Category    string `db:"category" json:"category"`
}

type AboutYielder interface {
	ByMostRecentYield() ([]YieldPayload, error)
	UpdateImage(about *About) error
}

type AboutDB interface {
	Create(about *About) error

	AboutYielder
}

// Chain
type (
	//AboutService will be returned by newAboutService function exposing
	// all methods available.
	AboutService interface {
		AboutDB
	}

	// All runtime validation and normalization nedded.
	aboutValidator struct {
		AboutDB
	}

	aboutDB struct {
		db *sqlx.DB
	}
)

func newAboutService(db *sqlx.DB) AboutService {
	return &aboutValidator{
		AboutDB: &aboutDB{
			db,
		},
	}
}

func (av *aboutValidator) Create(about *About) error {
	if err := runAboutValFuncs(about,
		av.normalize,
		av.titleRequired,
		av.descLess10240,
		av.categoryLess64,
	); err != nil {
		return err
	}

	return av.AboutDB.Create(about)
}

func (av *aboutValidator) UpdateImage(about *About) error {
	if err := runAboutValFuncs(about,
		av.idRequired,
		av.thumbnailRequired,
	); err != nil {
		return err
	}
	return av.AboutDB.UpdateImage(about)
}

func (av *aboutValidator) normalize(about *About) error {
	about.Title = strings.TrimSpace(about.Title)
	about.Description = strings.TrimSpace(about.Description)
	about.Category = strings.TrimSpace(about.Category)

	return nil
}

func (av *aboutValidator) titleRequired(about *About) error {
	if utf8.RuneCountInString(about.Title) < 3 {
		return errors.ErrTitleTooShort
	}

	if utf8.RuneCountInString(about.Title) > 256 {
		return errors.ErrTitleTooLong
	}
	return nil
}

func (av *aboutValidator) descLess10240(about *About) error {
	if about.Description == "" {
		return nil
	}

	if utf8.RuneCountInString(about.Description) > 10240 {
		return errors.ErrAboutDescTooLong
	}

	return nil
}

func (av *aboutValidator) categoryLess64(about *About) error {

	if about.Category == "" {
		return nil
	}

	if utf8.RuneCountInString(about.Category) > 64 {
		return errors.ErrAboutCatTooLong
	}

	return nil
}

func (av *aboutValidator) idRequired(about *About) error {
	if about.ID == 0 {
		return errors.ErrIDInvalid
	}
	return nil
}

func (av *aboutValidator) thumbnailRequired(about *About) error {
	if about.Thumbnail == "" {
		return errors.ErrThumbRequired
	}
	return nil
}

func (ad *aboutDB) Create(about *About) error {
	stmt := `INSERT INTO about (created_at, updated_at, deleted_at, title, description, thumbnail, category)
			 VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id;`

	err := ad.db.Get(&about.ID,
		stmt,
		time.Now(),
		time.Now(),
		nil,
		about.Title,
		about.Description,
		about.Thumbnail,
		about.Category,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.ErrNotFound
		}
		return err
	}

	return nil
}

func (ad *aboutDB) ByMostRecentYield() ([]YieldPayload, error) {
	stmt := `SELECT created_at AS date, title, description, thumbnail, category FROM about
	WHERE deleted_at IS NULL
	ORDER BY date ASC;`
	rows, err := ad.db.Queryx(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var payload []YieldPayload
	for rows.Next() {
		row := make(YieldPayload)
		if err := rows.MapScan(row); err != nil {
			return nil, err
		}
		payload = append(payload, row)
	}

	return payload, nil
}

func (ad *aboutDB) UpdateImage(about *About) error {
	stmt := `UPDATE about SET thumbnail=$1 WHERE id = $2`

	_, err := ad.db.Exec(stmt, about.Thumbnail, about.ID)
	return err
}

type aboutValFunc func(about *About) error

func runAboutValFuncs(about *About, fns ...aboutValFunc) error {
	for _, fn := range fns {
		if err := fn(about); err != nil {
			return err
		}
	}
	return nil
}
