package models

import (
	"path"
	"testing"
	"time"
)

var date = time.Date(2009, 11, 17, 20, 0, 0, 0, time.UTC)

var validVideo = &Video{
	Model:    Model{ID: 1, CreatedAt: date, UpdatedAt: date},
	Filename: "sampleFile.mp4",
	Md5:      MD5,
	Size:     1024,
	TusdID:   TusdID,
	UserID:   1,
	Status:   Done,
}

func Test_getFile(t *testing.T) {
	tests := []videoTestTable{
		{name: "Valid file name", video: validVideo, wantErr: false, want: TusdID},
		{name: "Invalid file name", video: &Video{}, wantErr: true, want: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getFile(tt.video)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getFileWithoutDataPrfix(t *testing.T) {
	tests := []videoTestTable{
		{name: "Valid without data prefix", video: validVideo, wantErr: false, want: path.Join(MD5, TusdID)},
		{name: "Invalid without data prefix", video: &Video{}, wantErr: true, want: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getFileWithoutDataPrfix(tt.video)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFileWithoutDataPrfix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getFileWithoutDataPrfix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getFileLocation(t *testing.T) {
	tests := []videoTestTable{
		{name: "Valid file location", video: validVideo, wantErr: false, want: path.Join(data, TusdID)},
		{name: "Invalid file location", video: &Video{}, wantErr: true, want: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getFileLocation(tt.video)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFileLocation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getFileLocation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getDir(t *testing.T) {
	tests := []videoTestTable{
		{name: "Valid dir", video: validVideo, wantErr: false, want: path.Join(data, MD5)},
		{name: "Invalid dir", video: &Video{}, wantErr: true, want: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getDir(tt.video)
			if (err != nil) != tt.wantErr {
				t.Errorf("getDir() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getDir() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getFinalFileLocation(t *testing.T) {
	tests := []videoTestTable{
		{name: "Valid final file local", video: validVideo, wantErr: false, want: path.Join(data, MD5, TusdID)},
		{name: "Invalid final file local", video: &Video{}, wantErr: true, want: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getFinalFileLocation(tt.video)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFinalFileLocation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getFinalFileLocation() = %v, want %v", got, tt.want)
			}
		})
	}
}
