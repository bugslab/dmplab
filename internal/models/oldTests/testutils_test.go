package models

import (
	"crypto/rand"
	"dmplab/pkg/argon2id"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jmoiron/sqlx"
)

const (
	Password = "12345678"
	TusdID   = "f95fb35ac69feeda452c7354b772252a"
	MD5      = "7eb06281330e967f6982f58574aef988"
	// data   = "data/"
)

type TestDB struct {
	testdb *sqlx.DB
}

var db TestDB

func TestMain(m *testing.M) {

	rand

	const (
		host = "localhost"
		port = 5432
		user = "postgres"
		pass = "123"
		name = "dmplab_test"
	)

	connInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, pass, name)

	dbt, err := sqlx.Open("postgres", connInfo)
	if err != nil {
		log.Fatalln(err)
	}

	schema, err := ioutil.ReadFile("../../scripts/init.sql")
	if err != nil {
		log.Fatalln("failed to load init.sql: ", err)

	}

	dbt.MustExec(string(schema))

	tx := dbt.MustBegin()
	tx.MustExec("INSERT INTO users (first_name, last_name, email) VALUES ($1, $2, $3)", "Jason", "Moiron", "jmoiron@jmoiron.net")
	tx.MustExec("INSERT INTO person (first_name, last_name, email) VALUES ($1, $2, $3)", "John", "Doe", "johndoeDNE@gmail.net")
	tx.MustExec("INSERT INTO place (country, city, telcode) VALUES ($1, $2, $3)", "United States", "New York", "1")
	tx.MustExec("INSERT INTO place (country, telcode) VALUES ($1, $2)", "Hong Kong", "852")
	tx.MustExec("INSERT INTO place (country, telcode) VALUES ($1, $2)", "Singapore", "65")
	// Named queries can use structs, so if you have an existing struct (i.e. person := &Person{}) that you have populated, you can pass it in as &person
	tx.NamedExec("INSERT INTO person (first_name, last_name, email) VALUES (:first_name, :last_name, :email)", &Person{"Jane", "Citizen", "jane.citzen@example.com"})
	tx.Commit()

	err = dbt.AutoMigrate(&User{}, &Gallery{}, &Video{}).Error
	if err != nil {
		log.Fatalln("cannot open a db connection: ", err)

	}
	db.testdb = dbt

	flag.Parse()
	exitCode := m.Run()

	err = db.testdb.DropTableIfExists(&User{}, &Gallery{}, &Video{}).Error
	if err != nil {
		log.Println(err)
	}

	db.testdb.Close()
	os.Exit(exitCode)
}

func (tdb *TestDB) teardown(t *testing.T) {
	err := tdb.testdb.DropTableIfExists(&User{}, &Gallery{}, &Video{}).Error
	if err != nil {
		t.Error(err)
	}
}

func newTestUser(t *testing.T) *User {

	pwstr, err := argon2id.CreateHash(Password, argon2id.DefaultParams)
	if err != nil {
		t.Fatal(err)
	}

	u := &User{
		Name:         "bob",
		Email:        "bob@sample.com",
		PasswordHash: pwstr,
		RememberHash: "FXsnsjqb3my53m8SdXaZmishlfKPle5T-MsEjtwM5WI=",
	}
	return u
}

func newTestVideo(t *testing.T) *Video {
	v := &Video{
		Filename: "sample.mp4",
		Md5:      []byte(MD5),
		Size:     1024,
		Status:   Done,
		TusdID:   TusdID,
		UserID:   1,
	}
	return v
}

// safeFilename is a helper function to generato safe characters ASCII names for files.
// it will return a random string containing letters and numbers a-zA-Z0-9
func safeFilename(ln int) string {

	if ln == 0 {
		return ""
	}

	str := make([]byte, ln)
	gap := ln / 3

	for i := 0; i < gap && ln >= 3; i += 3 {
		str[i] = randomChar('A', 'Z')
		str[i+1] = randomChar('a', 'b')
		str[i+2] = randomChar('0', '9')
	}
	for j := ln % 3; j != 0; j-- {
		str[ln-j] = randomChar('a', 'z')
	}
	return string(str)
}

func randomChar(begin rune, end rune) uint8 {
	rand.Seed(time.Now().UnixNano())
	return uint8(rand.Int31n(end-begin+1) + begin)
}
