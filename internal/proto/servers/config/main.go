package main

import (
	"context"
	"dmplab/internal/proto/contracts/config"
	"fmt"
	"net"
	"os"

	"github.com/golang/protobuf/jsonpb"

	"google.golang.org/grpc"
)

type Server struct {
	config.Config
}

func main() {

	f, err := os.Open("./config/.config")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	c := new(Server)
	if err := jsonpb.Unmarshal(f, c); err != nil {
		panic(err)
	}

	listener, err := net.Listen("tcp", ":3003")
	if err != nil {
		panic(err)
	}
	defer listener.Close()

	gs := grpc.NewServer()
	config.RegisterConfigServiceServer(gs, c)

	if err := gs.Serve(listener); err != nil {
		panic(err)
	}
}

func (s *Server) GetAllcfg(ctx context.Context, _ *config.Void) (*config.Config, error) {
	return &s.Config, nil
}

func (s *Server) GetAPIcfg(ctx context.Context, _ *config.Void) (*config.AccessAddress, error) {
	return s.GetAPI(), nil
}

func (s *Server) GetUIcfg(ctx context.Context, _ *config.Void) (*config.AccessAddress, error) {
	return s.GetUI(), nil
}

func (s *Server) GetInitcfg(ctx context.Context, _ *config.Void) (*config.App, error) {
	return s.GetApp(), nil
}

func (s *Server) GetDatabasecfg(ctx context.Context, _ *config.Void) (*config.Database, error) {
	return s.GetDatabase(), nil
}

func (s *Server) GetLogDatabasecfg(ctx context.Context, _ *config.Void) (*config.Database, error) {
	return s.GetLogDatabase(), nil
}

func (s *Server) GetKeyscfg(ctx context.Context, _ *config.Void) (*config.Keys, error) {
	return s.GetKeys(), nil
}

func (s *Server) GetRclonecfg(ctx context.Context, _ *config.Void) (*config.Rclone, error) {
	return s.GetRclone(), nil
}

func (s *Server) GetRRCcfg(ctx context.Context, _ *config.Void) (*config.RcloneRemoteControl, error) {
	return s.Rclone.GetRRC(), nil
}

func (s *Server) GetRScriptcfg(ctx context.Context, _ *config.Void) (*config.RcloneScript, error) {
	return s.Rclone.GetRScript(), nil
}

func (s *Server) GetDBAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {

	addr := new(config.AddrReply)

	addr.Addr = fmtDBConn(
		s.Database.GetHost(),
		s.Database.GetPort(),
		s.Database.GetUser(),
		s.Database.GetPass(),
		s.Database.GetName(),
		s.Database.GetSSL(),
	)

	return addr, nil

}

func (s *Server) GetLogDBAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {
	addr := new(config.AddrReply)

	addr.Addr = fmtDBConn(
		s.LogDatabase.GetHost(),
		s.LogDatabase.GetPort(),
		s.LogDatabase.GetUser(),
		s.LogDatabase.GetPass(),
		s.LogDatabase.GetName(),
		s.LogDatabase.GetSSL(),
	)

	return addr, nil
}

func (s *Server) GetAPIAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {
	addr := new(config.AddrReply)
	addr.Addr = fmt.Sprintf("%s:%d", s.API.GetHost(), s.API.GetPort())

	return addr, nil
}

func (s *Server) GetUIAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {
	addr := new(config.AddrReply)
	addr.Addr = fmt.Sprintf("%s:%d", s.UI.GetHost(), s.UI.GetPort())

	return addr, nil
}

func (s *Server) GetRcloneKeys(ctx context.Context, _ *config.Void) (*config.RcloneKeys, error) {
	return s.Rclone.GetRcloneKeys(), nil
}

func (s *Server) GetRcloneAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {
	addr := new(config.AddrReply)
	addr.Addr = fmt.Sprintf("%s:%d", s.Rclone.GetHost(), s.Rclone.GetPort())

	return addr, nil
}

func (s *Server) GetRcloneRemoteControlAddr(ctx context.Context, _ *config.Void) (*config.AddrReply, error) {
	addr := new(config.AddrReply)

	addr.Addr = fmt.Sprintf("%s:%d", s.Rclone.RRC.GetHost(), s.Rclone.RRC.GetPort())

	return addr, nil
}

func fmtDBConn(host string, port int32, user, pass, name, ssl string) string {

	var str string

	if pass == "" {
		str = fmt.Sprintf(
			"host=%s port=%d user=%s dbname=%s sslmode=%s",
			host, port, user, name, ssl,
		)

	} else {

		str = fmt.Sprintf(
			"host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
			host, port, user, pass, name, ssl,
		)
	}

	return str
}

