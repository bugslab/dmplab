package main

import (
	"context"
	"log"
	"net"
	"sync"

	"dmplab/internal/proto/clients/config"
	contract "dmplab/internal/proto/contracts/config"
	"dmplab/internal/proto/contracts/logger"

	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"
	"google.golang.org/grpc"
)

var void = new(logger.Void)
var cfgvoid = new(contract.Void)

type logService struct {
	db       *sqlx.DB
	mx       sync.Mutex
	channels []chan *logger.Message
}

func main() {
	listener, err := net.Listen("tcp", ":3004")
	if err != nil {
		panic(err)
	}
	defer listener.Close()
	gserver := grpc.NewServer()

	var cs []chan *logger.Message

	logserv := &logService{
		db:       newDBLog(),
		channels: cs,
	}

	logger.RegisterLoggerServiceServer(gserver, logserv)

	dbc := make(chan *logger.Message)
	defer close(dbc)

	logserv.channels = append(logserv.channels, dbc)

	go logserv.saveLog(dbc)

	if err := gserver.Serve(listener); err != nil {
		panic(err)
	}
}

func (ls *logService) StreamLog(vd *logger.Void, streamer logger.LoggerService_StreamLogServer) error {

	ctx := streamer.Context()

	st := make(chan *logger.Message)
	ls.mx.Lock()
	ls.channels = append(ls.channels, st)
	ls.mx.Unlock()
	defer close(st)

	// Query 20 most recent logs and send over the stream connection
	logs, err := ls.getLogs()
	if err != nil {
		log.Printf("failed to query values  from database: %v", err)
	}

	for _, msg := range logs {
		err := streamer.Send(msg)
		if err != nil {
			log.Printf("An error accurred! %v", err)
		}
	}

	// wait for new messages comming from the channel and send it over the stream connection
	for {
		// exit if context is done
		select {
		case <-ctx.Done():
			return ctx.Err()

		case msg := <-st:
			err := streamer.Send(msg)
			if err != nil {
				log.Printf("An error accurred! %v", err)
			}
		}

		// update max and send it to stream
	}
}

func (ls *logService) SendLog(ctx context.Context, in *logger.Message) (*logger.Void, error) {

	for idx, c := range ls.channels {
		select {
		case <-c:
			// remove closed channel
			ls.mx.Lock()
			ls.channels = append(ls.channels[:idx], ls.channels[idx+1:]...)
			ls.mx.Unlock()
		default:
			c <- in
		}
	}

	return void, nil
}

func newDBLog() *sqlx.DB {

	db, err := sqlx.Connect(config.GetLogDatabasecfg().GetDialect(), config.GetLogDBAddr())
	if err != nil {
		panic(err)
	}

	return db
}

func (ls *logService) saveLog(msgc <-chan *logger.Message) {

	stmt := `INSERT INTO log (data) VALUES ($1);`

	for msg := range msgc {
		_, err := ls.db.Exec(stmt, msg.GetP())
		if err != nil {
			log.Printf("Failed to save log in the database: %v", err)
		}
	}
}

func (ls *logService) getLogs() ([]*logger.Message, error) {

	stmt := `SELECT data FROM log ORDER BY id DESC LIMIT 20`

	rows, err := ls.db.Queryx(stmt)
	if err != nil {
		return nil, err
	}

	var msgs []*logger.Message

	for rows.Next() {

		row := struct {
			P types.JSONText `db:"data"`
		}{P: types.JSONText{}}
		rows.Scan(&row.P)
		if err != nil {
			return nil, err
		}

		msg := &logger.Message{P: row.P}
		msgs = append(msgs, msg)
	}

	return msgs, nil
}
