package cli

import (
	"log"
	"os"
)

var stdout = log.New(os.Stdout, "[tusd] ", log.Ldate|log.Ltime|log.Lshortfile)
var stderr = log.New(os.Stderr, "[tusd] ", log.Ldate|log.Ltime|log.Lshortfile)

func logEv(logOutput *log.Logger, eventName string, details ...string) {
	LogEvent(logOutput, eventName, details...)
}

func LogEvent(logger *log.Logger, eventName string, details ...string) {
	result := make([]byte, 0, 100)

	result = append(result, `event="`...)
	result = append(result, eventName...)
	result = append(result, `" `...)

	for i := 0; i < len(details); i += 2 {
		result = append(result, details[i]...)
		result = append(result, `="`...)
		result = append(result, details[i+1]...)
		result = append(result, `" `...)
	}

	result = append(result, "\n"...)
	logger.Output(3, string(result))
}
