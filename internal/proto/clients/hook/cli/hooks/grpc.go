package hooks

import (
	"context"
	"time"

	"dmplab/internal/proto/contracts/hook"

	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	"google.golang.org/grpc/metadata"
	"fmt"
	"github.com/tus/tusd/pkg/handler"
	"google.golang.org/grpc"
)

type GrpcHook struct {
	Endpoint       string
	MaxRetries     int
	Backoff        int
	Client         hook.HookServiceClient
	ForwardHeaders []string
}

func unaryInterceptor(ctx context.Context,
method string,
req, reply interface{},
cc *grpc.ClientConn,
invoker grpc.UnaryInvoker,
opts ...grpc.CallOption) error{

	fmt.Println("CLIENT: ")
	ctx = metadata.AppendToOutgoingContext(ctx, "authorization", "dmp_token_HERE")

	fmt.Println("<---unaryinterceptor", method)
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		for k, v := range md {
			fmt.Printf("%v: %v\n", k, v)
		}
	}
	fmt.Println("outgoing")
	out, pass := metadata.FromOutgoingContext(ctx)
	if pass {
		for k, v := range out {
			fmt.Printf("%v: %v\n", k, v)
	}
}
	fmt.Println("unaryinterceptor--->")

	return invoker(ctx, method, req, reply, cc, opts...)
}

func (g *GrpcHook) Setup() error {
	opts := []grpc_retry.CallOption{
		grpc_retry.WithBackoff(grpc_retry.BackoffLinear(time.Duration(g.Backoff) * time.Second)),
		grpc_retry.WithMax(uint(g.MaxRetries)),
	}
	grpcOpts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(grpc_retry.UnaryClientInterceptor(opts...)),
		grpc.WithUnaryInterceptor(unaryInterceptor),
	}
	conn, err := grpc.Dial(g.Endpoint, grpcOpts...)
	if err != nil {
		return err
	}
	g.Client = hook.NewHookServiceClient(conn)
	return nil
}

func (g *GrpcHook) InvokeHook(typ HookType, info handler.HookEvent, captureOutput bool) ([]byte, int, error) {
	ctx := context.Background()
	hk := marshal(typ, info)

	if vals, ok := info.HTTPRequest.Header["Cookie"]; ok {
		hk.HttpRequest.Cookies = append(hk.HttpRequest.Cookies, vals...)
	}

	req := &hook.SendRequest{Hook: hk}

	resp, err := g.Client.Send(ctx, req)
	if err != nil {
		if e, ok := (err).(HookError); ok {
			return nil, e.StatusCode(), err
		}
		return nil, 2, err
	}
	if captureOutput {
		return resp.Response.GetValue(), 0, err
	}
	return nil, 0, err
}

func marshal(typ HookType, info handler.HookEvent) *hook.Hook {

	return &hook.Hook{
		Upload: &hook.Upload{
			Id:             info.Upload.ID,
			Size:           info.Upload.Size,
			SizeIsDeferred: info.Upload.SizeIsDeferred,
			Offset:         info.Upload.Offset,
			MetaData:       info.Upload.MetaData,
			IsPartial:      info.Upload.IsPartial,
			IsFinal:        info.Upload.IsFinal,
			PartialUploads: info.Upload.PartialUploads,
			Storage:        info.Upload.Storage,
		},
		HttpRequest: &hook.HTTPRequest{
			Method:     info.HTTPRequest.Method,
			Uri:        info.HTTPRequest.URI,
			RemoteAddr: info.HTTPRequest.RemoteAddr,
		},
		Name: string(typ),
	}
}
