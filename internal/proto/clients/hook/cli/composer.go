package cli

import (
	"os"
	"path/filepath"

	"github.com/tus/tusd/pkg/filelocker"
	"github.com/tus/tusd/pkg/filestore"
	"github.com/tus/tusd/pkg/handler"
)

var Composer *handler.StoreComposer

func CreateComposer() {

	Composer = handler.NewStoreComposer()
	dir, err := filepath.Abs(Flags.UploadDir)
	if err != nil {
		stderr.Fatalf("Unable to make absolute path: %s", err)
	}

	stdout.Printf("Using '%s' as directory storage.\n", dir)
	if err := os.MkdirAll(dir, os.FileMode(0774)); err != nil {
		stderr.Fatalf("Unable to ensure directory exists: %s", err)
	}

	store := filestore.New(dir)
	store.UseIn(Composer)

	locker := filelocker.New(dir)
	locker.UseIn(Composer)

	stdout.Printf("Using %.2fMB as maximum size.\n", float64(Flags.MaxSize)/1024/1024)
}
