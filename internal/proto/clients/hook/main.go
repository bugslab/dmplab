package main

import (
	"dmplab/internal/proto/clients/hook/cli"
)

func main() {
	cli.ParseFlags()
	cli.PrepareGreeting()

	cli.CreateComposer()
	cli.Serve()
}
