FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp/config \
        && mkdir -p /srv/dmp

COPY ./deploy/caddy /srv/dmp
COPY ./config/Caddyfile /srv/dmp/config
COPY ./certs /root/certs

RUN chown -R dmp:dmp /srv
RUN chown -R dmp:dmp /root/certs
# USER dmp:dmp

EXPOSE 80 443 2015

ENTRYPOINT ["/srv/dmp/caddy"]
CMD ["--conf", "/srv/dmp/config/Caddyfile", "--log", "stdout"]
