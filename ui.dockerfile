FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp/config \
        && mkdir -p /srv/dmp/public \
        && mkdir -p /srv/dmp

COPY ./config/.config /srv/dmp/config/
COPY ./deploy/ui  /srv/dmp/

RUN chown -R dmp:dmp /srv/dmp

EXPOSE 3000

CMD sleep 7 && ./ui

USER dmp
