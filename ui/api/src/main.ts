import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Method } from "axios";
import axios from "./utils/axios-instance";
import { User } from "@/types";
import vuetify from "./plugins/vuetify";
import parseISO from "date-fns/parseISO";
import format from "date-fns/format";
import vuetimeline from "@growthbunker/vuetimeline";

router.beforeEach((to, _, next) => {
  let method: Method = "GET";

  if (store.getters.getID > 0) {
    method = "HEAD";
  }

  axios
    .request({ method: method, url: "/auth" })
    .then(resp => {
      let user: User;
      if (method === "GET") {
        user = resp.data;
      }

      if (
        resp.status === 200 &&
        (to.path === "/login" || to.path === "/register")
      ) {
        store.commit("SET_AUTH", true);

        if (method === "GET") {
          store.commit("SET_USER", user);
        }

        next({ name: "home" });
      } else if (resp.status === 200) {
        if (method === "GET") {
          store.commit("SET_USER", user);
        }

        store.commit("SET_AUTH", true);
        next();
      } else {
        store.commit("SET_AUTH", false);
        next({
          name: "login",
          query: { redirectFrom: to.fullPath }
        });
      }
    })
    .catch(_ => {
      store.commit("SET_AUTH", false);
      if (to.path !== "/login" && to.path !== "/register") {
        next({
          name: "login",
          query: { redirectFrom: to.fullPath }
        });
      } else {
        next();
      }
    });
});

Vue.filter("toHumanSize", function(value: number): string {
  if (value > 1e9) {
    return (value / 1024 / 1024 / 1024).toFixed(2) + "GB";
  }

  return (value / 1024 / 1024).toFixed(2) + "MB";
});

Vue.filter("toHumanDate", function(value: number): string {
  if (!value) {
    return "Jan 01, 1701";
  }

  return format(parseISO(value.toString()), "PP");
});

Vue.use(vuetimeline);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
