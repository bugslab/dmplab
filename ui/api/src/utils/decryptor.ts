import axios from "./axios-instance";
import Rclone from "rclonets";

class Decryptor {
  private static instance: Decryptor;
  private _rc: Rclone;

  public static getInstance() {
    if (this.instance) {
      return this.instance;
    }
    this.instance = new Decryptor();
    return this.instance;
  }

  get getRef() {
    return this._rc;
  }

  public static destroy() {
    this.instance = undefined;
  }

  private constructor() {
    axios
      .get("/credentials")
      .then(resp => {
        const data = resp.data.yield;
        this._rc = new Rclone(data.Pass, data.Salt);
      })
      .catch(err => {
        // reject(err);
        console.log(err);
      });
  }
}

const rcInit = () => {
  Decryptor.getInstance();
};
const rcDestroy = () => {
  Decryptor.destroy();
};

const rcRef = () => {
  if (!Decryptor.getInstance().getRef) {
    rcInit();
  }
  return Decryptor.getInstance().getRef;
};

export default { rcInit, rcDestroy, rcRef };
