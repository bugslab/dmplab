import Vue from "vue";
import Vuex from "vuex";
import upload from "../modules/upload";
import auth from "../modules/auth";
import snackbar from "../modules/snackbar";
import progressbar from "../modules/progressBar";
import video from "../modules/video";
import userVideos from "../modules/userVideos";
import decrypt from "../modules/decrypt";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    upload,
    auth,
    video,
    decrypt,
    snackbar,
    userVideos,
    progressbar
  }
});
