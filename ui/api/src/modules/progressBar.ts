import { ProgressBar, RootState } from "@/types";
import { Module, ActionTree, MutationTree, GetterTree } from "vuex";

const state: ProgressBar = {
  value: 10,
  show: true
};

const mutations: MutationTree<ProgressBar> = {
  SET_PROGRESS_BAR_VALUE(state, value: number) {
    state.value = value;
  },

  SET_PROGRESS_BAR_SHOW(state, show: boolean) {
    state.show = show;
  }
};

const actions: ActionTree<ProgressBar, RootState> = {
  showProgress({ commit }) {
    commit("SET_PROGRESS_BAR_SHOW", true);
  },

  closeProgress({ commit }) {
    commit("SET_PROGRESS_BAR_SHOW", false);

  },

  updatePrgressValue({ commit }, value: number) {
    commit("SET_PROGRESS_BAR_VALUE", value);
  },

};

const getters: GetterTree<ProgressBar, RootState> = {
  showProgress: state => {
    return state.show;
  },

  getProgressValue: state => {
      return state.value;
  }
};

const module: Module<ProgressBar, RootState> = {
  state,
  mutations,
  actions,
  getters
};

export default module;

