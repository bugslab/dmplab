import {
  UserVideoState,
  RootState,
  MediaVideo,
  Status,
  StatusType
} from "@/types";
import axios from "../utils/axios-instance";
import router from "@/router";
import { MutationTree, ActionTree, GetterTree } from "vuex";

const state: UserVideoState = {
  videos: [] as Array<MediaVideo>
};

const mutations: MutationTree<UserVideoState> = {
  SET_USER_VIDEO(state, video: MediaVideo) {
    state.videos.push(video);
  },

  CLEAR_USER_VIDEOS(state) {
    state.videos = [];
  }
};

const actions: ActionTree<UserVideoState, RootState> = {
  async fetchVideoByID({ state, commit, dispatch }, { vm, hash }) {
    let found = state.videos.find(video => {
      return video.md5 == hash;
    });

    if (found === undefined) {
      await axios
        .get("/videos/" + hash)
        .then(resp => {
          found = resp.data.yield;
        })
        .catch(_ => {
          const errStatus = {
            text: "Invalid requested file",
            type: StatusType.error
          } as Status;
          dispatch("snackbar", errStatus);
          router.push("/");
        });
      found && commit("SET_USER_VIDEO", found);
    }

    if (found === undefined) return;

    if (found.blob_screen_link === undefined || found.blob_screen_link == "") {
      this.dispatch("blobToImg", found.screen_pub_link)
        .then(blobURL => {
          vm.$set(found, "blob_screen_link", blobURL);
        })
        .catch(_ => {
          vm.$set(found, "blob_screen_link", "/assets/err.jpeg");
        });
    }

    if (found.blob_thumb_link === undefined || found.blob_thumb_link == "") {
      this.dispatch("blobToImg", found.thumb_pub_link)
        .then(blobURL => {
          vm.$set(found, "blob_thumb_link", blobURL);
        })
        .catch(err => {
          console.log(err);
          vm.$set(found, "blob_thumb_link", "/assets/err.jpeg");
        });
    }
  },

  fetchLatestUserVideos({ commit, state }, { vm, userid }) {
    axios
      .get("/uservideos", { params: { userid: userid } })
      .then(resp => {
        const data: Array<MediaVideo> = resp.data.yield;

        data.forEach(item => {
          let found = state.videos.find(video => {
            return video.md5 == item.md5;
          });

          if (found !== undefined && found.locked) return;

          if (
            found === undefined ||
            found.blob_thumb_link === undefined ||
            found.blob_thumb_link == ""
          ) {
            this.dispatch("blobToImg", item.thumb_pub_link)
              .then(blob => {
                vm.$set(item, "blob_thumb_link", blob);
                vm.$set(item, "locked", false);
              })
              .catch(_ => {
                vm.$set(item, "blob_thumb_link", "/assets/err.jpeg");
                vm.$set(item, "locked", false);
              });

            if (found === undefined) {
              item.locked = true;
              commit("SET_USER_VIDEO", item);
            }
          }
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
};

const getters: GetterTree<UserVideoState, RootState> = {
  getVideosByUserID: state => (id: number): Array<MediaVideo> => {
    return state.videos.filter(video => video.user_id == id);
  },

  getVideoByID: state => (hash: string): MediaVideo => {
    let src = state.videos.find(video => video && video.md5 == hash);
    if (src === undefined) {
      src = { filename: "loading..." } as MediaVideo;
    }
    return src;
  }
};

const module = {
  state,
  mutations,
  actions,
  getters
};

export default module;
