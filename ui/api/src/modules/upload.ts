import { UploadState, RootState, Status, StatusType } from "@/types";
import { Module, ActionTree, MutationTree, GetterTree } from "vuex";
import { ArrayBuffer as MD5ArrayBuffer } from "spark-md5";
import tus from "tus-js-client";
import { mdiExclamationThick, mdiAlert, mdiCheck } from "@mdi/js";

const state: UploadState = {
  uploading: false,
  filename: "",
  md5: "",
  status: { type: StatusType.success, text: "", icon: "" },
  progress: 0,
  md5progress: 0
};

const mutations: MutationTree<UploadState> = {
  SET_UPLOADING(state, isUploading: boolean) {
    state.uploading = isUploading;
  },
  SET_FILENAME(state, filename: string) {
    state.filename = filename;
  },

  SET_MD5(state, md5: string) {
    state.md5 = md5;
  },

  SET_STATUS(state, status: Status) {
    status.icon =
      status.type === StatusType.success
        ? mdiCheck
        : status.type === StatusType.warning
        ? mdiAlert
        : mdiExclamationThick;
    state.status = status;
  },

  SET_PROGRESS(state, progress: number) {
    state.progress = progress;
  },
  SET_MD5_PROGRESS(state, progress: number) {
    state.md5progress = progress;
  }
};

const actions: ActionTree<UploadState, RootState> = {
  startUpload({ commit, dispatch }, { file, info }) {
    // reset
    commit("SET_UPLOADING", true);
    // this.updateFilename("");
    // this.updateMD5("");
    commit("SET_STATUS", {});
    commit("SET_PROGRESS", 0);
    commit("SET_MD5_PROGRESS", 0);

    // @ts-ignore
    const blobSlice = File.prototype.slice;

    const chunkSize = 2097152; // Read in chunks of 2MB
    const chunks = Math.ceil(file.size / chunkSize);
    const spark = new MD5ArrayBuffer();
    let currentChunk = 0;

    commit("SET_FILENAME", file.name);

    const fileReader = new FileReader();

    fileReader.onload = ev => {
      commit("SET_MD5_PROGRESS", Math.ceil((currentChunk / chunks) * 100));
      // @ts-ignore
      spark.append(ev.target.result);

      currentChunk++;

      if (currentChunk < chunks) {
        loadNext();
      } else {
        commit("SET_MD5_PROGRESS", 100);
        const hash = spark.end(); // Compute hash
        commit("SET_MD5", hash);

        const upload = new tus.Upload(file, {
          endpoint: "https://upload." + window.location.hostname + "/files",
          withCredentials: true,
          retryDelays: [0, 3000, 5000],
          metadata: {
            filename: file.name,
            filetype: file.type,
            md5: hash,
            title: info.title,
            desc: info.desc,
            tags: info.tags
          },

          onError: err => {
            commit("SET_UPLOADING", false);
            const messageRegex = /response text: ([a-zA-Z0-9\s!.,:-]+)/gm;
            const msg = messageRegex.exec(err.message)[1].trim() || "";

            const errStatus = {
              text:
                msg === undefined || msg === "" ? "Something went wrong!" : msg,
              type: StatusType.error
            } as Status;

            commit("SET_STATUS", errStatus);
            dispatch("snackbar", errStatus);
          },

          onProgress: (bytesUploaded, bytesTotal) => {
            const percentage = Math.ceil((bytesUploaded / bytesTotal) * 100);
            commit("SET_PROGRESS", percentage);
          },

          onSuccess: () => {
            commit("SET_UPLOADING", false);
            const sucStatus = {
              text: "File has been successfully uploaded",
              type: StatusType.success
            } as Status;

            commit("SET_STATUS", sucStatus);
            dispatch("snackbar", sucStatus);
          }
        });

        upload.start();
      }
    };

    fileReader.onerror = () => {
      commit("SET_UPLOADING", false);

      const errStatus = {
        text: "Something went wrong!",
        type: StatusType.error
      } as Status;

      commit("SET_STATUS", errStatus);
      dispatch("snackbar", errStatus);
    };

    function loadNext() {
      const start = currentChunk * chunkSize;
      const end =
        start + chunkSize >= file.size ? file.size : start + chunkSize;
      fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
    }

    loadNext();
  }
};

const getters: GetterTree<UploadState, RootState> = {
  uploading: (state): boolean => {
    return state.uploading;
  },

  status: (state): Status => {
    return state.status;
  },

  progress: (state): number => {
    return state.progress;
  },

  md5progress: (state): number => {
    return state.md5progress;
  },

  filename: (state): string => {
    return state.filename;
  },
  md5: (state): string => {
    return state.md5;
  }
};

const module: Module<UploadState, RootState> = {
  state,
  mutations,
  actions,
  getters
};

export default module;
