FROM alpine:3.11

RUN apk update \
        && apk upgrade \
        && apk add --no-cache libc6-compat

WORKDIR /srv/dmp

RUN addgroup -g 1000 dmp \
        && adduser -u 1000 -G dmp -s /bin/sh -D dmp \
        && mkdir -p /srv/dmp/public \
        && mkdir -p /srv/dmp

COPY ./deploy/api /srv/dmp/

RUN chown -R dmp:dmp /srv/dmp

EXPOSE 3001

CMD sleep 6 && ./api

USER dmp
